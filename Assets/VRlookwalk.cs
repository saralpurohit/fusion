﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRlookwalk : MonoBehaviour {

	public Transform Gaze;

	public float thresholdAngle = 10.0f;

	public float velocity = 3.0f;

	public bool moveforward;

	private CharacterController cc;


	void Start () {
		cc = GetComponent<CharacterController> ();
		
	}
	
	void Update () {
		if (Gaze.eulerAngles.x >= thresholdAngle && Gaze.eulerAngles.x < 90.0f) {
		
			moveforward = true;
			}

		else {
			moveforward = false;
		}

		if (moveforward == true) {
			Vector3 forward = Gaze.TransformDirection(Vector3.forward);
			cc.SimpleMove (forward * velocity); 
			//Debug.Log ("rotation threshold reached");
		}
	}

}
