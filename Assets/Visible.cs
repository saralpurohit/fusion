﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Visible : MonoBehaviour {

	public Transform Gaze;

	public float thresholdAngle = 340.0f;

	public GameObject boop;
	public GameObject beep;
	public GameObject bang;
	//public bool active=false;


	/* void Update ()
	{
		if (Gaze.eulerAngles.x <= thresholdAngle && Gaze.eulerAngles.x > 300.0f) 
		{
			boop.SetActive(true);
			beep.SetActive(true);
			bang.SetActive(true);
			Debug.Log ("buttons unlocked");
			active = true;
		}

		if (active == true && (Gaze.eulerAngles.x <= thresholdAngle && Gaze.eulerAngles.x > 300.0f)) 
		{

			boop.SetActive(false);
			beep.SetActive(false);
			bang.SetActive(false);
			Debug.Log ("buttons locked");
		}
		//Debug.Log (Gaze.eulerAngles.x);
		*/

	bool firstlook = true;
	bool active = false;
	bool prevActive = false;
	// Update is called once per frame
	void Update () {

		if (Gaze.eulerAngles.x <= thresholdAngle && Gaze.eulerAngles.x > 300.0f)
		{
			if(firstlook == true)
			{
				firstlook = false;

				active = !active;
			}
		}
		if (Gaze.eulerAngles.x <= thresholdAngle && Gaze.eulerAngles.x < 300.0f)
		{
			firstlook = true;
		}
		if (prevActive != active)
		{
			boop.SetActive(active);
			beep.SetActive(active);
			bang.SetActive(active);
			Debug.Log("buttons active " + active);
		}
		prevActive = active;
	}


	}
			

